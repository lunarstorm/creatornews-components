const MailChimpFormHTML = `<link
href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css"
rel="stylesheet"
type="text/css"
/>
<div id="mc_embed_signup" style="margin:0 auto;">

<script>
 function getMCEmail(){
   window.location.href = '/pages/reader-sign-up-web?email=' + document.querySelector('#mc-embedded-subscribe-form input[type="email"]').value
   localStorage.setItem('mailchimp-clicked',true);
   localStorage.setItem('mailchimp-clicked__expiry',Date.now() + 3600 * 24);
 }
</script>

<form
  action="https://creatornews.us5.list-manage.com/subscribe/post?u=d13a033838c667a633d5127e6&amp;id=d30e1ffab8"
  method="post"
  id="mc-embedded-subscribe-form"
  name="mc-embedded-subscribe-form"
  style="width:100%"
  class="validate"
  target="_blank"
  novalidate
>
  <div class="d-flex flex-nowrap w-100">
        <input
        type="email"
        value=""
        name="EMAIL"
        class="form-control"
        id="mce-EMAIL"
        placeholder="Enter Your Email"
        required
      />
      <a
        name="subscribe"
        id="mc-embedded-subscribe"
        class="btn btn-primary"
        style="white-space: nowrap"
        onClick="getMCEmail()"
      >
        Subscribe 🔔
      </a>
      </div>
    </div>
    <div id="mce-responses" class="clear" style="order:12;width:100%;">
        <div class="response" id="mce-error-response" style="display:none;margin-top:0.5rem;"></div>
        <div class="response" id="mce-success-response" style="display:none;margin-top:0.5rem;"></div>
    </div>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="height:0;width:0;position: absolute; left: -5000px" aria-hidden="true">
      <input
        type="text"
        name="b_d13a033838c667a633d5127e6_d30e1ffab8"
        tabindex="-1"
        value=""
      />
</form>
</div>`

const prepareMailChimpForm = () => {
    window.fnames = new Array();
    window.ftypes = new Array();
    fnames[0] = "EMAIL";
    ftypes[0] = "email";
    let mcValidate = document.createElement("script");
    mcValidate.setAttribute(
        "src",
        "//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js"
    );
    document.head.appendChild(mcValidate);
}

export {
    MailChimpFormHTML,
    prepareMailChimpForm
}