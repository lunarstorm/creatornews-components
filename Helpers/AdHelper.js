
class AdHelper {

    static adsEnabled = true;

    static positionHasAd(position, start) {
        if (!AdHelper.adsEnabled) {
            return false;
        }

        // First Ad after the 3rd item
        if (position === start) {
            return true;
        }

        // Ads after every n'th item thereafter
        if (position > start) {
            let nth = 8;
            return (position - start) % nth === 0;
        }

        return false;
    }

    /**
     *
     * Quick & Dirty Helper to "wait for function to be available"
     * via https://stackoverflow.com/questions/10328885/i-need-a-generic-javascript-wait-for-function-to-be-available
     *
     * @param {*} fnName
     * @param {*} args
     */
    static waitForFn(fnName, args) {
        var fn;
        try {
            eval("fn = " + fnName);
            if (fn) {
                fn.apply(null, args);
            } else {
                setTimeout(function () { AdHelper.waitForFn(fnName, args); }, 50);
            }
        } catch (e) {
            setTimeout(function () { AdHelper.waitForFn(fnName, args); }, 50);
        }
    }

    static invokeAdVendor() {
        if (!AdHelper.adsEnabled) {
            console.log('Notice: Ads currently disabled');
            return false;
        }

        //console.log("Cn.invokeAdVendor (calling window.atdRenderAds())");
        AdHelper.waitForFn('window.atdRenderAds');
    }

}

export default AdHelper;
